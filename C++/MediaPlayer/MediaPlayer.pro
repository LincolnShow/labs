#-------------------------------------------------
#
# Project created by QtCreator 2016-04-09T15:45:43
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MediaPlayer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    dial.h

FORMS    += mainwindow.ui
