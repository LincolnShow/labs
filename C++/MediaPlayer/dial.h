#ifndef DIAL_H
#define DIAL_H
#include <QDial>

class Dial : public QDial
{
    Q_OBJECT
public:
    Dial(QWidget* parent = 0) : QDial(parent){
        connect(this, SIGNAL(actionTriggered(int)), this, SLOT(onAction(int)));
    }

protected slots:
    void onAction(int val){
        static const int minDistance = 2;
        if (val == QAbstractSlider::SliderMove) {
        if (value() == maximum() && sliderPosition()<maximum()-minDistance) {
                this->setSliderPosition(maximum());
            } else if (value() == minimum() && sliderPosition()>minimum()+minDistance){
                this->setSliderPosition(minimum());
            }
        }
    }
};

#endif // DIAL_H
