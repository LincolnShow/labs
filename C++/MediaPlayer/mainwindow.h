#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtMultimedia/QMediaPlayer>
#include <QFileDialog>
#include <string>
#include "dial.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pb_choose_clicked();

    void on_pb_prev_clicked();

    void on_pb_play_clicked();

    void on_dial_volume_valueChanged(int value);

    void on_pb_pause_clicked();

    void on_pb_stop_clicked();

    void on_pb_next_clicked();

private:
    QMediaPlayer player;
    QStringList playlist;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
