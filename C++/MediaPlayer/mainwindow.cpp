#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QtMultimedia/QMediaPlayer"
using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    player.setVolume(50);
    ui->dial_volume->setValue(50);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pb_choose_clicked()
{
    ui->list_play->clear();
    string s;
    int pos;
    playlist = QFileDialog::getOpenFileNames(this, "Choose audio files", "", "Audio (*.mp3)");
    for(int i = 0; i < playlist.size(); ++i){
        s = playlist[i].toStdString();
        pos = s.find_last_of("/");
        s = s.substr(pos + 1);
        ui->list_play->addItem(s.c_str());
        }
}

void MainWindow::on_pb_prev_clicked()
{
    player.stop();
    int row = ui->list_play->currentRow();
    ui->list_play->setCurrentRow(row != 0 ? row-1 : row);
    on_pb_play_clicked();
}

void MainWindow::on_pb_play_clicked()
{
    ui->dial_volume->setWrapping(1);
    if(!playlist.isEmpty() && ui->list_play->currentRow() >= 0){
        if(player.state() != 2){
            player.setMedia(QMediaContent(QUrl::fromLocalFile(playlist[ui->list_play->currentRow()])));
        }
        player.play();
    }
}

void MainWindow::on_dial_volume_valueChanged(int value)
{
    player.setVolume(value);
}

void MainWindow::on_pb_pause_clicked()
{
    player.pause();
}

void MainWindow::on_pb_stop_clicked()
{
    player.stop();
}

void MainWindow::on_pb_next_clicked()
{
    player.stop();
    int row = ui->list_play->currentRow();
    ui->list_play->setCurrentRow(row+1 != ui->list_play->count() ? row+1 : row);
    on_pb_play_clicked();
}
