#include "automata.h"

Automata::Automata(std::string filepath)
{
    QString path = filepath.c_str();
    QFile* xfile = new QFile(path);
    if(!xfile->open(QIODevice::ReadOnly | QIODevice::Text)){
        path = QFileDialog::getOpenFileName(nullptr, "Select an XML file", "", "XML (*.xml)");
        xfile->open(QIODevice::ReadOnly | QIODevice::Text);
    }
    QXmlStreamReader xml(xfile);
    QXmlStreamReader::TokenType token;
    while(!xml.atEnd() && !xml.hasError()){
        token = xml.readNext();
        if(token == xml.StartDocument){
            continue;
        }
        if(token == xml.StartElement){
            QXmlStreamAttributes attrs = xml.attributes();
            if(attrs.hasAttribute("name"))
                this->menu.push_back(attrs.value("name").toString().toStdString());
            if(attrs.hasAttribute("price"))
                this->prices.push_back(attrs.value("price").toFloat());
        }
    }
    cash = 0.0f;
    state = WAIT;
}
void Automata::on(){
    if(state == OFF)
        state = WAIT;
}
void Automata::off(){
    if(state != OFF)
        state = OFF;
}
bool Automata::coin(const float value){
    if((state == WAIT || state == ACCEPT) && value > 0.0f){
        state = ACCEPT;
        cash += value;
        return true;
    }
    return false;
}
std::vector<std::string>* Automata::getNames(){
    std::vector<std::string>* vmenu = new std::vector<std::string>(menu);
    return vmenu;
}

std::vector<float> *Automata::getPrices()
{
    std::vector<float>* vprices = new std::vector<float>();
    for(unsigned int i = 0; i < prices.size(); ++i){
        vprices->push_back(prices[i]);
    }
    return vprices;
}
std::string Automata::getState(){
        switch (state){
        case OFF:
            return "Off";
        case WAIT:
            return "Waiting";
        case ACCEPT:
            return "Accepting cash";
        case CHECK:
            return "Checking the sum of the order";
        case COOK:
            return "Making drink";
        }
        return "NULL";
}

std::string Automata::getDrinkName(unsigned int index)
{
    if(index <= menu.size()){
        return menu[index];
    }
    return "";
}
bool Automata::choice(unsigned int index){
    if(state != OFF){
        if((index < menu.size()) && check(index)){
            cash -= prices[index];
            cook();
            return true;
        }
        else{
            state = WAIT;
            return false;
        }
    }
    return false;
}
bool Automata::check(const unsigned int index){
    state = CHECK;
    return prices[index] > cash ? false : true;
}
float Automata::cancel(){
    if(state != OFF){
        state = WAIT;
        float moneyback = cash;
        cash = 0.0f;
        return moneyback;
    }
    return 0.0f;
}
void Automata::cook(){
    state = COOK;
    finish();
}
void Automata::finish(){
    state = WAIT;
}
float Automata::getBalance(){
    return cash;
}
