#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QStyle>
#include <QDesktopWidget>
using namespace std;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(this->width(), this->height());
    machine = new Automata("pricelist.xml");
    ui->le_balance->setText(QString::number(machine->getBalance()) + " RUB");
    string state = machine->getState();
    ui->le_state->setText(state.c_str());
    vector<string>* names = machine->getNames();
    vector<float>* prices = machine->getPrices();
    for(unsigned int i = 0; i < names->size(); ++i){
        ui->list_names->addItem(names->at(i).c_str());
    }
    for(unsigned int i = 0; i < prices->size(); ++i){
        ui->list_prices->addItem(QString::number(prices->at(i)) + " RUB");
    }
    val = 0;
    qt = new QTimer();
    connect(this->qt, SIGNAL(timeout()), this, SLOT(timer()));
    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_list_names_itemSelectionChanged()
{
    string drink = machine->getDrinkName(ui->list_names->currentRow());
    ui->le_drinkName->setText(drink.c_str());
}

void MainWindow::on_pb_buy_clicked()
{
    if(ui->bar_pour->value()){
        ui->le_state->setText("Another drink is inside");
    }
    else if(machine->choice(ui->list_names->currentRow())){
        updateBalance();
        ui->le_state->setText("Filling the cup");
        this->qt->setInterval(58);
        this->qt->start();
        player.setMedia(QUrl::fromLocalFile(QDir::currentPath() + "/makesound.mp3"));
        player.play();
    }
    else{
        ui->le_state->setText("Not enough money");
    }
}

void MainWindow::on_pb_take_clicked()
{
    if(ui->bar_pour->value() == 100){
        ui->bar_pour->setValue(0);
        ui->le_state->setText("Waiting");
    }
    else
        ui->le_state->setText("The drink is not ready yet");
}

void MainWindow::on_pb_coin5_clicked()
{
    this->machine->coin(5.0f);
    updateBalance();
}

void MainWindow::updateBalance()
{
    ui->le_balance->setText(QString::number(this->machine->getBalance(), 'f', 2) + " RUB");
}

void MainWindow::on_pb_coin10_clicked()
{
    this->machine->coin(10.0f);
    updateBalance();
}

void MainWindow::on_pb_coin50_clicked()
{
    this->machine->coin(50.0f);
    updateBalance();
}

void MainWindow::on_pb_coin100_clicked()
{
    this->machine->coin(100.0f);
    updateBalance();
}

void MainWindow::timer()
{
    if(val <= 100){
        ui->bar_pour->setValue(val++);
    }
    else{
        val = 0;
        this->qt->stop();
        ui->le_state->setText("Your drink is ready");
        player.stop();
    }
}

void MainWindow::on_pb_cancel_clicked()
{
    if(this->machine->getBalance()){
        this->machine->cancel();
        ui->le_state->setText("Purchase cancelled");
        updateBalance();
    }
    else
        ui->le_state->setText("Waiting");
}
