#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QtMultimedia>
#include "automata.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_list_names_itemSelectionChanged();

    void on_pb_buy_clicked();

    void on_pb_take_clicked();

    void on_pb_coin5_clicked();

    void on_pb_coin10_clicked();

    void on_pb_coin50_clicked();

    void on_pb_coin100_clicked();

    void timer();

    void on_pb_cancel_clicked();

private:
    QMediaPlayer player;
    QTimer *qt;
    short val;
    Ui::MainWindow *ui;
    Automata *machine;
    void updateBalance();
};

#endif // MAINWINDOW_H
