#ifndef VKACCESS_H
#define VKACCESS_H
#include <QtCore>

class VkAccess
{
public:
    VkAccess();
    void authorize();
protected:
    QString token;
    unsigned int user_id;
};

#endif // VKACCESS_H
