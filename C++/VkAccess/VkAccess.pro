QT += core
QT -= gui

CONFIG += c++11

TARGET = VkAccess
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    vkaccess.cpp

HEADERS += \
    vkaccess.h
