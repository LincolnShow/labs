#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "circle.h"
#include "datetime.h"
#include <QMessageBox>
#include <QLineEdit>
#define WALKPATH_PRICE 1000.0
#define FENCE_PRICE 2000.0
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void on_pb_radius_clicked();

    void on_pb_ference_clicked();

    void on_pb_area_clicked();

    void on_pb_recalc2_clicked();

    void on_pb_recalc3_clicked();

    void on_pb_today_clicked();

    void on_pb_today_2_clicked();

    void on_pb_past_clicked();

    void on_pb_future_clicked();

    void on_pb_set_clicked();

    void on_pb_set_2_clicked();

    void on_pb_past_2_clicked();

    void on_pb_future_2_clicked();

    void on_pushButton_clicked();

private:
    void fillData1();
    void fillData2();
    void updateString(QLineEdit& stringDate, DateTime &date);
    Ui::MainWindow *ui;
    QMessageBox message;
    Circle demo, earth, rope, pool, walkpath;
    DateTime *date1, *date2;
};

#endif // MAINWINDOW_H
