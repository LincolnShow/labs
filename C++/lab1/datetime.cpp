#include "datetime.h"

using namespace std;
DateTime::DateTime()
{
    time(&TIME);
}
DateTime::DateTime(int day, int month, int year, int hour, int min, int sec){
    tm tmp;
    tmp.tm_mday = day;
    tmp.tm_mon = month-1;
    tmp.tm_year = year - 1900;
    tmp.tm_hour = hour+1;
    tmp.tm_min = min;
    tmp.tm_sec = sec;
    TIME = mktime(&tmp);
}
DateTime::DateTime(DateTime &src){
    TIME = src.TIME;
}
tm* DateTime::getToday(){
    return localtime(&TIME);
}
string DateTime::getStringDate(){
    return *new string(ctime(&TIME));
}
tm* DateTime::getFuture(int N){
    if(N<0)
        N = 0;
    TIME += (N * 86400);
    return localtime(&TIME);
}
tm* DateTime::getPast(int N){
    if(N<0)
        N = 0;
    TIME -= (N * 86400);
    return localtime(&TIME);
}
int DateTime::getMonth(){
    return localtime(&TIME)->tm_mon;
}
int DateTime::getMonthDay()
{
    return localtime(&TIME)->tm_mday;
}
int DateTime::getYear()
{
    return localtime(&TIME)->tm_year;
}
int DateTime::getHour()
{
    return localtime(&TIME)->tm_hour;
}
int DateTime::getMinutes()
{
    return localtime(&TIME)->tm_min;
}
int DateTime::getSeconds()
{
    return localtime(&TIME)->tm_sec;
}
unsigned int DateTime::getDifference(const DateTime& date2){
    return this->TIME > date2.TIME ? ((this->TIME - date2.TIME)) / 86400 : ((this->TIME - date2.TIME)) / 86400;
}
