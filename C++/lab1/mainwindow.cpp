#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(this->width(), this->height());
    //2
    earth = *new Circle(6378.1);
    rope = *new Circle(6378.1);
    ui->le_earthR->setText(QString::number(earth.getRadius()));
    ui->le_ropeR->setText(QString::number(rope.getRadius()));
    ui->le_extraRope->setText("0");
    ui->le_gapValue->setText("0");
    //3
    pool = *new Circle();
    walkpath = *new Circle();
    //4
    date1 = new DateTime();
    date2 = new DateTime();
    fillData1();
    fillData2();
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::fillData1()
{
    ui->le_year->setText(QString::number(date1->getYear() + 1900));
    ui->le_month->setText(QString::number(date1->getMonth() + 1));
    ui->le_day->setText(QString::number(date1->getMonthDay()));
    ui->le_hour->setText(QString::number(date1->getHour()));
    ui->le_minutes->setText(QString::number(date1->getMinutes()));
    ui->le_seconds->setText(QString::number(date1->getSeconds()));
    updateString(*ui->le_stringDate, *date1);
}
void MainWindow::fillData2(){
    ui->le_year_2->setText(QString::number(date2->getYear() + 1900));
    ui->le_month_2->setText(QString::number(date2->getMonth() + 1));
    ui->le_day_2->setText(QString::number(date2->getMonthDay()));
    ui->le_hour_2->setText(QString::number(date2->getHour()));
    ui->le_minutes_2->setText(QString::number(date2->getMinutes()));
    ui->le_seconds_2->setText(QString::number(date2->getSeconds()));
    updateString(*ui->le_stringDate_2, *date2);
}

void MainWindow::updateString(QLineEdit& stringDate, DateTime& date)
{
    stringDate.setText(date.getStringDate().c_str());
}
void MainWindow::on_pb_radius_clicked()
{
        QString rad = ui->le_radius->text();
        demo.setRadius(rad.toDouble());
        ui->le_ference->setText(QString::number(demo.getFerence(), 'f', 2));
        ui->le_area->setText(QString::number(demo.getArea(), 'f', 2));
}

void MainWindow::on_pb_ference_clicked()
{
        QString fer = ui->le_ference->text();
        demo.setFerence(fer.toDouble());
        ui->le_radius->setText(QString::number(demo.getRadius(), 'f', 2));
        ui->le_area->setText(QString::number(demo.getArea(), 'f', 2));
}

void MainWindow::on_pb_area_clicked()
{
        QString area = ui->le_area->text();
        demo.setArea(area.toDouble());
        ui->le_radius->setText(QString::number(demo.getRadius(), 'f', 2));
        ui->le_ference->setText(QString::number(demo.getFerence(), 'f', 2));
}

void MainWindow::on_pb_recalc2_clicked()
{
    double extra_rope = ui->le_extraRope->text().toDouble();
    rope.setRadius(6378.1);
    double diff = 0.0;
    ui->le_gapValue->setText("0");
    if(extra_rope > 0.0){
        rope.setFerence(rope.getFerence() + extra_rope);
        ui->le_ropeR->setText(QString::number(rope.getRadius(), 'f', 2));
        diff = (rope.getRadius() - earth.getRadius());
        ui->le_gapValue->setText(QString::number(diff, 'f', 2));
    }

}

void MainWindow::on_pb_recalc3_clicked()
{
    double walkpath_cost = 0.0, fence_cost = 0.0;
    double walkpath_width = ui->le_path->text().toDouble(), pool_radius = ui->le_pool->text().toDouble();
    if(walkpath_width >= 0.0 && pool_radius > 0){
        pool.setRadius(pool_radius);
        walkpath.setRadius(ui->le_path->text().toDouble() + pool.getRadius());
        walkpath_cost = ((walkpath.getArea() - pool.getArea()) * WALKPATH_PRICE);
        fence_cost = walkpath.getFerence() * FENCE_PRICE;
    }
    ui->le_fenceCost->setText(QString::number(fence_cost, 'f', 2) + "  rub.");
    ui->le_pathCost->setText(QString::number(walkpath_cost, 'f', 2) + "  rub.");
    ui->le_totalCost->setText(QString::number(fence_cost + walkpath_cost, 'f', 2) + "  rub.");
}

void MainWindow::on_pb_today_clicked()
{
    date1 = new DateTime();
    fillData1();
}
void MainWindow::on_pb_today_2_clicked()
{
    date2 = new DateTime();
    fillData2();
}

void MainWindow::on_pb_past_clicked()
{
    date1->getPast(ui->le_days->text().toInt());
    fillData1();
}

void MainWindow::on_pb_future_clicked()
{
    date1->getFuture(ui->le_days->text().toInt());
    fillData1();
}

void MainWindow::on_pb_set_clicked()
{
    date1 = new DateTime(ui->le_day->text().toInt(), ui->le_month->text().toInt(), ui->le_year->text().toInt(),
                         ui->le_hour->text().toInt(), ui->le_minutes->text().toInt(), ui->le_seconds->text().toInt());
    updateString(*ui->le_stringDate, *date1);
}

void MainWindow::on_pb_set_2_clicked()
{
    date2 = new DateTime(ui->le_day_2->text().toInt(), ui->le_month_2->text().toInt(), ui->le_year_2->text().toInt(),
                         ui->le_hour_2->text().toInt(), ui->le_minutes_2->text().toInt(), ui->le_seconds_2->text().toInt());
    updateString(*ui->le_stringDate, *date1);
}

void MainWindow::on_pb_past_2_clicked()
{
    date2->getPast(ui->le_days_2->text().toInt());
    fillData2();
}

void MainWindow::on_pb_future_2_clicked()
{
    date2->getFuture(ui->le_days_2->text().toInt());
    fillData2();
}

void MainWindow::on_pushButton_clicked()
{
    message.setText("Days difference: " + QString::number(date1->getDifference(*date2)));
    message.exec();
}
