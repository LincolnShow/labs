/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QComboBox *comboBox;
    QTableWidget *table_items;
    QPushButton *pb_clear;
    QLineEdit *le_itemData;
    QPushButton *pb_add;
    QPushButton *pb_delete;
    QSpinBox *sb_priority;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(413, 339);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        comboBox = new QComboBox(centralWidget);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(275, 9, 111, 31));
        table_items = new QTableWidget(centralWidget);
        if (table_items->columnCount() < 2)
            table_items->setColumnCount(2);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        table_items->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        table_items->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        table_items->setObjectName(QStringLiteral("table_items"));
        table_items->setGeometry(QRect(10, 10, 241, 301));
        table_items->setLayoutDirection(Qt::LeftToRight);
        table_items->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        table_items->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        table_items->setSizeAdjustPolicy(QAbstractScrollArea::AdjustIgnored);
        table_items->setAutoScroll(true);
        table_items->setRowCount(0);
        table_items->setColumnCount(2);
        table_items->horizontalHeader()->setVisible(true);
        table_items->horizontalHeader()->setCascadingSectionResizes(false);
        table_items->horizontalHeader()->setDefaultSectionSize(170);
        table_items->horizontalHeader()->setMinimumSectionSize(50);
        table_items->horizontalHeader()->setProperty("showSortIndicator", QVariant(false));
        table_items->horizontalHeader()->setStretchLastSection(true);
        table_items->verticalHeader()->setVisible(true);
        table_items->verticalHeader()->setDefaultSectionSize(20);
        table_items->verticalHeader()->setMinimumSectionSize(20);
        table_items->verticalHeader()->setStretchLastSection(false);
        pb_clear = new QPushButton(centralWidget);
        pb_clear->setObjectName(QStringLiteral("pb_clear"));
        pb_clear->setGeometry(QRect(280, 40, 101, 26));
        le_itemData = new QLineEdit(centralWidget);
        le_itemData->setObjectName(QStringLiteral("le_itemData"));
        le_itemData->setGeometry(QRect(268, 140, 81, 26));
        pb_add = new QPushButton(centralWidget);
        pb_add->setObjectName(QStringLiteral("pb_add"));
        pb_add->setGeometry(QRect(275, 169, 111, 26));
        pb_delete = new QPushButton(centralWidget);
        pb_delete->setObjectName(QStringLiteral("pb_delete"));
        pb_delete->setGeometry(QRect(275, 197, 111, 26));
        sb_priority = new QSpinBox(centralWidget);
        sb_priority->setObjectName(QStringLiteral("sb_priority"));
        sb_priority->setGeometry(QRect(350, 140, 46, 26));
        sb_priority->setMaximum(999);
        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "PQueue demo", 0));
        comboBox->setCurrentText(QString());
        QTableWidgetItem *___qtablewidgetitem = table_items->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("MainWindow", "Value", 0));
        QTableWidgetItem *___qtablewidgetitem1 = table_items->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("MainWindow", "Priority", 0));
        pb_clear->setText(QApplication::translate("MainWindow", "Delete Queue", 0));
        pb_add->setText(QApplication::translate("MainWindow", "Add Item", 0));
        pb_delete->setText(QApplication::translate("MainWindow", "Delete Item", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
