#include "PhoneBook.h"
using namespace std;

PhoneBook::PhoneBook(string file_path) {
    string h = file_path;
    ifstream datafile(file_path, ifstream::in);
    int n;
    char buf[1024] = { 0 };
    string s;
    while(datafile.good()){
        datafile.getline(buf, sizeof(buf), '\n');
        if((n = atoi(buf))) {
            s = string(buf);
            s = s.substr(s.find_first_of(' ') + 1);
            book.insert(std::pair<unsigned int, string>(n, s));
        }
    }
}

PhoneBook::~PhoneBook() {
}

void PhoneBook::insert(unsigned int number, std::string name) {
    book.insert(std::pair<unsigned int, string>(number, name));
}

std::string PhoneBook::findNameOf(unsigned int number) {
    map<unsigned int, string>::iterator iter = book.begin();
    while(iter != book.end()){
        if(iter->first == number)
            return iter->second;
        ++iter;
    }
    return "";
}

unsigned int PhoneBook::findNumberOf(std::string name) {
    map<unsigned int, string>::iterator iter = book.begin();
    while(iter != book.end()){
        if(iter->second == name)
            return iter->first;
        ++iter;
    }
    return 0;
}

PhoneBook::PhoneBook() {

}

std::map<unsigned int, std::string> PhoneBook::getSurnameNumbers(std::string surname) {
    map<unsigned int, string> results;
    map<unsigned int, string>::iterator iter = book.begin();
    while(iter != book.end()){
        if(iter->second.find(surname) != string::npos)
            results.insert(*iter);
        ++iter;
    }
    return results;
}

std::map<unsigned int, std::string> PhoneBook::getPhoneBook() {
    return book;
}

std::list<unsigned int> PhoneBook::getNumbers() {
    map<unsigned int, string>::iterator iter = book.begin();
    list<unsigned int> results;
    while(iter != book.end()){
        results.push_back(iter->first);
        ++iter;
    }
    return results;
}

std::list<std::string> PhoneBook::getNames() {
    list<string> results;
    map<unsigned int, string>::iterator iter = book.begin();
    while(iter != book.end()){
        results.push_back(iter->second);
        ++iter;
    }
    return results;
}
