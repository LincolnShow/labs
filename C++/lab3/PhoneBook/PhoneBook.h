#ifndef PHONEBOOK_PHONEBOOK_H
#define PHONEBOOK_PHONEBOOK_H


#include <map>
#include <fstream>
#include <cstdlib>
#include <list>
class PhoneBook {
public:
    PhoneBook();
    PhoneBook(std::string file_path);
    ~PhoneBook();
    void insert(unsigned int number, std::string name);
    std::string findNameOf(unsigned int number);
    unsigned int findNumberOf(std::string name);
    std::map<unsigned  int, std::string> getSurnameNumbers(std::string surname);
    std::map<unsigned  int, std::string> getPhoneBook();
    std::list<unsigned int> getNumbers();
    std::list<std::string> getNames();
private:
    std::map <unsigned int, std::string> book;
};
#endif //PHONEBOOK_PHONEBOOK_H