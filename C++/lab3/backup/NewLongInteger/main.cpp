#include <iostream>
#include "longinteger.h"
using namespace std;

int main()
{
    //cout << "INT: " << pow(2, 16) << endl;
    LongInteger n1("2");
    string s1 = n1.getValue();
    LongInteger n2("10000");
    string s2 = n2.getValue();
    LongInteger n3;
    clock_t start, end;
    start = clock();
    n3 = n1 ^ n2;
    end = clock();
    cout << "Time elapsed: " << (double) (end - start)/CLOCKS_PER_SEC << " sec." << endl;
    cout << s1 << " ^ " << s2 << " = " << n3.getValue() << endl;
    //getchar();
    return 0;
}
