#ifndef LONGINTEGER_H
#define LONGINTEGER_H
#include <string>
#include <iostream>
#include <string.h>
#include <algorithm>
#include <locale>
class LongInteger {
public:
    std::string getValue() const;
    ~LongInteger();
    LongInteger();
    LongInteger(std::string numString);
    LongInteger(const LongInteger& src);
    void change(std::string numString);
    LongInteger operator+(LongInteger& sum);
    LongInteger operator++();
    LongInteger operator++(int);
    inline LongInteger operator+=(LongInteger& sum);
    LongInteger operator-(LongInteger& sub);
    LongInteger operator--();
    LongInteger operator--(int);
    inline LongInteger operator-=(LongInteger& sub);
    LongInteger operator*(LongInteger& fac);
    LongInteger operator*=(LongInteger& fac);
    LongInteger operator/(LongInteger& div);
    LongInteger operator/=(LongInteger& div);
    LongInteger operator%(LongInteger& div);
    LongInteger operator^(LongInteger& pow);
    bool operator<(LongInteger& lint);
    bool operator<=(LongInteger& lint);
    bool operator>(LongInteger& lint);
    bool operator>=(LongInteger& lint);
    bool operator==(LongInteger& lint);
    bool operator==(const char* snum);
    bool operator!=(LongInteger& lint);
private:
    LongInteger pow(LongInteger x, LongInteger n);
    LongInteger add(LongInteger& n1, LongInteger& n2);
    LongInteger deduct(LongInteger& n1, LongInteger& n2);
    void adjust();
    bool negative = false;
    std::string digits;
};

inline bool isWiden(const char& c);
#endif // LONGINTEGER_H
