TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
INCLUDEPATH += .
SOURCES += main.cpp \
    longinteger.cpp

HEADERS += \
    longinteger.h
