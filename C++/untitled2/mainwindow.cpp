#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <string>
using namespace std;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(&webWin, SIGNAL(urlChanged(const QUrl&)), this, SLOT(newUrl(const QUrl&)));
    connect(this, SIGNAL(loginSuccess()), this, SLOT(onLoginSucess()));
    connect(this, SIGNAL(redirection(const QUrl&)), this, SLOT(handleRedirection(const QUrl&)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pb_login_clicked()
{
    if(token.isEmpty()){
        QString link("https://oauth.vk.com/authorize?"
                 "client_id=5431531&display=page&redirect_uri=https://oauth.vk.com/blank.html&"
                 "scope=audio&response_type=token&v=5.50&revoke=1");
        url = link;
        webWin.load(url);
        webWin.show();
    }
    else{
        emit onLoginSucess();
    }
}

void MainWindow::handleRedirection(const QUrl& u)
{
    ui->status->setText("Receiving user data..");
    string s = u.url().toStdString();
    token = s.substr(s.find_first_of('=')+1, (s.find_first_of('&') - s.find_first_of('=')-1)).c_str();
    //s = s.substr(s.find_last_of('=')+1);
    user_id = s.substr(s.find_last_of('=')+1).c_str();
    emit loginSuccess();
}

void MainWindow::newUrl(const QUrl &u)
{
    if(u.url().contains("access_token")){
        webWin.close();
        emit redirection(u);
    }
}

void MainWindow::slotReadyRead()
{
    ui->status->setText("Receiving reply..");
    storage.append(reply->readAll());
    ui->pbar->setValue(storage.size());
}

void MainWindow::onLoginSucess()
{
    ui->status->setText("Sending request..");
    QNetworkRequest request;
    QString finalRequest("https://api.vk.com/method/audio.get?user_id="
                         + user_id + "&count=6000&v=5.50&access_token=" + token);
    request.setUrl(QUrl(finalRequest));
    reply = netman.get(request);
    connect(reply, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));
    connect(reply, SIGNAL(finished()), this, SLOT(replyFinished()));
    ui->pbar->setMaximum(1000000);
}

void MainWindow::replyFinished()
{
    reply->close();
    netman.clearAccessCache();
    webWin.close();
    QJsonDocument jsonDoc = QJsonDocument::fromJson(storage);
    storage.clear();
    //for_each(songsINFO.begin(), songsINFO.end(), [](QPair<QString,QString> pair){ pair.first.clear(); pair.second.clear();});
    songsINFO.clear();
    ui->list_songs->clear();
    if(jsonDoc.isObject()){
        ui->status->setText("Parsing reply..");
        QJsonObject jo = jsonDoc.object()["response"].toObject();
        QJsonArray ja = jo["items"].toArray();
        QJsonArray::iterator items_iter = ja.begin();
        QPair<QString, QString> song;
        ui->pbar->setMaximum(ja.size());
        while(items_iter != ja.end()){
            jo = (*items_iter).toObject();
            song.first = jo["url"].toString();
            song.second = jo["artist"].toString() + " - " + jo["title"].toString();
            songsINFO.push_back(song);
            ++items_iter;
            ui->pbar->setValue(songsINFO.size());
        }
        ui->status->setText("Filling playlist..");
        QVector<QPair<QString, QString>>::iterator info_iter = songsINFO.begin();
        while(info_iter != songsINFO.end()){
            ui->list_songs->addItem((*info_iter).second);
            ui->pbar->setValue(ui->list_songs->count());
            //(*info_iter).second.clear();
            ++info_iter;
        }
        ui->list_songs->setCurrentRow(0);
        ui->status->setText("FINISHED");
    }
}

void MainWindow::on_pushButton_clicked()
{
    if(!songsINFO.isEmpty()){
        QString path = songsINFO[ui->list_songs->currentRow()].first;
        player.stop();
        player.setMedia(QMediaContent(path));
        player.play();
    }
}
