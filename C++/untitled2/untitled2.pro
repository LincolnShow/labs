#-------------------------------------------------
#
# Project created by QtCreator 2016-04-24T00:30:26
#
#-------------------------------------------------

QT       += core gui webenginewidgets webengine multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = untitled2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp
INCLUDEPATH += .
HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
CONFIG += c++11
