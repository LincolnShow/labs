#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <vector>
#include <QMainWindow>
#include <QtWidgets>
#include <QtWebEngineWidgets>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QtMultimedia>
#include <QVector>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
signals:
    void loginSuccess();
    void redirection(const QUrl& u);
private slots:
    void handleRedirection(const QUrl& u);
    void newUrl(const QUrl& u);
    void on_pb_login_clicked();
    void slotReadyRead();
    void onLoginSucess();
    void replyFinished();
    void on_pushButton_clicked();

private:
    QMediaPlayer player;
    QUrl url;
    QVector<QPair<QString, QString>> songsINFO;
    QByteArray storage;
    QNetworkAccessManager netman;
    QNetworkReply* reply;
    QString token, user_id;
    QWebEngineView webWin;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
