/*
1. �������� ���������, �������������� ���������� ���� �� �����-
��� ������������� ������
*/
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#define SIZE 2000
int main() {
    char string[SIZE];
	puts("Enter a string:");
    fgets(string, SIZE, stdin);
    char startFound = 0;
    unsigned int wordCount = 0;
    for (int i = 0; i < strlen(string); i++) {
        char c = string[i];
        if (!isspace(c) && startFound == 0) {
            startFound = 1;
			wordCount++;
        }
        else if (isspace(c) && startFound == 1) {
            startFound = 0;
        }
    }
    printf("Words: %u\n", wordCount);
    return 0;
}