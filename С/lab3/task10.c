/*
10. �������� ���������, ������� ����������� � ������������ ������, ���������
�� ���������� ���� � ����� �����
n
, � ����� �������
n
- �� ����� �� ������. �
������ �������������
n
��������� ��������� �� ������
���������:
� ���������� ������ ��������� ������� ������ ���� ������� �� �������.
��������� �������������� ������� ������
*/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#define SIZE 2000

int main() {
    char string[SIZE];
    int N = 0;
    puts("Enter a text:");
    fgets(string, SIZE, stdin);
    puts("Enter the index of a word:");
    scanf("%d", &N);
    short startFound = 0, wordPrinted = 0;
    int start = -1, wordCount = 0;

    for (int i = 0; wordPrinted == 0; i++) {
        char c = string[i];
        if (c == 0)
            break;
        if (!isspace(c) && startFound == 0) {
            startFound = 1;
            start = i;
        }
        if ((isspace(c) || c == ',' || c == '.') && startFound == 1) {
            wordCount++;
            if (wordCount == N) {
                for (int q = start, copyID = i + 1; q < strlen(string); q++, copyID++) {
                    if (string[copyID] != 0) {
                        string[q] = string[copyID];
                    }
                    else {
                        string[q] = 0;
                        break;
                    }
                }
            }
            else {
                startFound = 0;
            }
        }
    }

    printf("%s\n", string);
    if (N > wordCount || N <= 0)
        puts("Wrong index.");
    return 0;
}