#include <stdio.h>
#include <string.h>
#include <stdlib.h>
typedef struct SYM {
    char ch;
    float freq;
} TSYM;
typedef TSYM * PSYM;
void buildFreqTable(PSYM syms, unsigned int size, FILE *fp);
unsigned int getUniques(FILE *fp);
int compare(const void *a, const void *b);
void printFreqTable(PSYM syms, int size);
int main(int argc, char *argv[]) {
    if (argc < 2) {
        puts("Usage: program_name file_path");
        exit(1);
    }
    FILE *fp = fopen(argv[1], "rt");
    int uniques = getUniques(fp);
    PSYM syms = (PSYM)calloc(sizeof(TSYM), uniques);
    buildFreqTable(syms, uniques, fp);
    qsort(syms, uniques, sizeof(TSYM), compare);
    printFreqTable(syms, uniques);
    return 0;
}
//���������� ������� �������������
void buildFreqTable(PSYM syms, unsigned int size, FILE *fp) {
    rewind(fp);
    char c;
    unsigned int total = 0;
    while ((c = fgetc(fp)) != EOF) {
        if (!isspace(c)) {
            for (unsigned int q = 0; q < size; q++) {
                if (!syms[q].ch) {
                    syms[q].ch = c;
                    syms[q].freq = 1.0f;
                    break;
                }
                else if (syms[q].ch == c) {
                    syms[q].freq += 1.0f;
                    break;
                }
            }
            total++;
        }
    }
    for (unsigned int i = 0; i < size; i++) {
        syms[i].freq /= total;
    }
}
//������� ������ ���������� ���������� �������� � �����
unsigned int getUniques(FILE *fp) {
    rewind(fp);
    unsigned char chars[256] = { 0 };
    char c;
    int uniques = 0;
    while ((c = fgetc(fp)) != EOF) {
        if (!isspace(c)) {
            if (!chars[c]) {
                chars[c] = 1;
                uniques++;
            }
        }
    }
    return uniques;
}
//����������
int compare(const void *a, const void *b) {
    return ((PSYM)a)->freq < ((PSYM)b)->freq ? 1 : 0;
}
//����� ������� �� �����
void printFreqTable(PSYM syms, int size) {
    for (int i = 0; i < size; i++) {
        printf("%c : %.4f\n", syms[i].ch, syms[i].freq);
    }
}