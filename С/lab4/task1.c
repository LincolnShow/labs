/*
1. �������� ���������, ������� ��������� ������������ ������ �������-
�� ����� � ����������, � ����� ��������� �� � ������� ��������-
��� ����� ������.
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define STRINGS 50
#define S_LENGTH 200
void compare(const void *a, const void *b) {
    return strlen(*(const char**)a) - strlen(*(const char**)b);
}
int main() {
    char strings[STRINGS][S_LENGTH] = { { 0 } };
    char *pointers[STRINGS] = { 0 };
    int line = 0;

    while (line < STRINGS &&
        *fgets(strings[line], S_LENGTH, stdin) != '\n') {
        pointers[line] = strings[line];
        line++;
    }
    qsort(pointers, line, sizeof(char*), compare);
    //��������
    for (int i = 0; i < line; i++)
    {
        printf("%s", pointers[i]);
    }
    return 0;
}