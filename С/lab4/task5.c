/*
5. �������� ���������, ����������� ������ (��. ������ 1), �� ������������
������, ����������� �� ���������� �����. ��������� ������ ��������� �����
������������ � ����.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define STRINGS 50
#define S_LENGTH 200
void compare(const void* a, const void* b) {
    return strlen(*(const char**)a) - strlen(*(const char**)b);
}
int main() {
    char strings[STRINGS][S_LENGTH] = { { 0 } };
    char path[S_LENGTH] = { 0 };
    char *pointers[STRINGS] = { 0 };
    FILE *IN;
    FILE *OUT;
    int line = 0, letter = 0;
    puts("Enter a file path in the following format \"C:\\file.txt\" (results will be saved in c:\\result.txt):");
    fgets(path, S_LENGTH, stdin);
    path[strlen(path) - 1] = 0;
    if (((IN = fopen(path, "r+")) == NULL) ||
        (OUT = fopen("C:\\result.txt", "w+")) == NULL) {
        perror("Error");
        return 1;
    }
    //������ ����� �� ����� � ��������� ������ strings; ���������� ���������� �� ������ � ������ pointers
    while (1) {
        char c = getc(IN);
        if (c == '\n' || c == EOF) {
            strings[line][letter] = '\n';
            pointers[line] = strings[line];
            line++;
            if (c == EOF)
                break;
            letter = 0;
        }
        else {
            strings[line][letter] = c;
            letter++;
        }
    }
    qsort(pointers, line, sizeof(char**), compare);
    //������ ����� �� ������� ���������� � ���� result.txt
    for (int i = 0; i < line; i++) {
        fputs(pointers[i], OUT);
    }
    fclose(IN);
    fclose(OUT);
    return 0;
}