/*
6. �������� ���������, ������� ����������� ���������� ������������� � �����,
� ����� ��������� ������ ��� ������������ � ��� �������. ��������� ������
���������� ������ �������� � ������ ������� ������������
*/
#include <stdio.h>
#define F_MEMBERS 30
#define NAME_SIZE 100
void cleanIn() {
    char c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}
int main() {
    int familySize;
    int *young[] = { 0 }, *old[] = { 0 };
    char names[F_MEMBERS][NAME_SIZE] = { 0 };
    int curAge = -1, youngestAge = -1, oldestAge = -1;
    while (1) {
        puts("Enter a number of family members:");
        if (scanf("%d", &familySize) != 1) {
            puts("Input error!");
            cleanIn();
        }
        else
            break;
    }
    for (int i = 0; i < familySize; i++)
    {
        while (1) {
            printf("Enter family member #%d's name and age (like \"John 20\"):\n", i + 1);
            if ((scanf("%s %d", names[i], &curAge)) != 2 || curAge <= 0 || curAge > 100) {
                puts("Input error!");
                cleanIn();
            }
            else {
                if (curAge > oldestAge) {
                    *old = names[i];
                    oldestAge = curAge;
                }
                if (curAge < youngestAge || youngestAge == -1) {
                    *young = names[i];
                    youngestAge = curAge;
                }
                break;
            }
        }
    }

    printf("Youngest person: %s - %d years old.\n  Oldest person: %s - %d years old.\n", *young, youngestAge, *old, oldestAge);
    return 0;
}
