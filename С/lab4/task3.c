/*
3. �������� ���������, ������� ����������� ������ � ����������, �� ��������
�� ������ ����������� (��������� �������� � ����� ������� � ������ ������)
*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define SIZE 256
int main() {
    char string[SIZE] = { 0 };
    puts("Enter a string: ");
    fgets(string, SIZE, stdin);
    if (string[0] == '\n')
        return -1;
    char *pStart = &string[0];
    while (isspace(*pStart)) {
        pStart++;
    }
    char *pEnd = &string[strlen(string) - 1];
    while (isspace(*pEnd)) {
        pEnd--;
    }
    int isPalindrome = 0;
    int x = 0;
    while (tolower(*pStart) == tolower(*pEnd) || (isspace(*pStart) || isspace(*pEnd))) {
        while (isspace(*pStart) || isspace(*pEnd)) {
            if (isspace(*pStart)) {
                pStart++;
            }
            if (isspace(*pEnd)) {
                pEnd--;
            }
        }
        if (tolower(*pStart) != tolower(*pEnd)) {
            isPalindrome = 0;
            break;
        }
        else {
            if ((pEnd - 1) >= (pStart + 1)) {
                pStart++;
                pEnd--;
            }
            else {
                isPalindrome = 1;
                break;
            }
        }
    }
    if (isPalindrome)
        puts("The string is palindrome.");
    else
        puts("The string is not palindrome!");
    return 0;
}