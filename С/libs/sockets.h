#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "user32.lib")
#include <stdio.h>
#include <winsock.h>
#include <errno.h>
typedef int SOCK;
const int WINSOCK_VERSION = 0x0101;

void startWinSock();
void stopWinSock();
void *hostName();
SOCK createSocket();
void removeSocket(SOCK*);
void bindSocket(SOCK*, const char *, const char *);
HWND getConsoleHWND();
void linkSocketWindow(SOCK*);
SOCK getWorkingServer(unsigned int);
char *getLocalIp();
unsigned long printLastError();
//������������� WinSock
void startWinSock(){
    WSADATA wsaData;

    if (WSAStartup(WINSOCK_VERSION, &wsaData))
    {
        printf("Winsock start failed!\n");
        WSACleanup();
    }
    else printf("Winsock start OK!\n");
}
//����������� ������� � WinSock
void stopWinSock() {
    if (WSACleanup())
        printf("ERROR: Winsock stop failed!\n");
    else
        printf("Winsock stop OK!\n");
}
//��������� ����� �����
char *hostName() {
    char chInfo[64];
    if (gethostname(chInfo, sizeof(chInfo)))
    {
        printf("ERROR: No local host!\n");
        return NULL;
    }
    else
    {
        printf("Host name: %s\n", chInfo);
        return chInfo;
    }
}
//�������� ������ ������ IPv4, ��� TCP/IP, �������� ������������� ���������� �����
SOCK createSocket()
{
    SOCK servsocket;
    servsocket = socket(PF_INET, SOCK_STREAM, 0);
    if (servsocket == INVALID_SOCKET)
    {
        printf("ERROR: Socket creation failed!\n");
        return NULL;
    }
    return servsocket;
}
//�������� � ��������� �������� ��������� ������
void removeSocket(SOCK *s) {
    closesocket(*s);
    *s = NULL;
}
//�������� ������ � ����� PORT � � ip-������ HOST
void bindSocket(SOCK *s, const char *HOST, const char *PORT) {
    SOCKADDR_IN socketaddr;
    HOSTENT *hostip;
    socketaddr.sin_family = AF_INET;
    socketaddr.sin_addr.s_addr = INADDR_ANY;
    socketaddr.sin_port = htons((unsigned short)atoi(PORT));
    hostip = gethostbyname(HOST);
    if (bind(*s, (LPSOCKADDR)&socketaddr, sizeof(socketaddr)) == SOCKET_ERROR)
    {
        printf("ERROR: Socket binding failed!\n");
        exit(1);
    }
    else printf("Socket binding OK!\n");
}
//�������� ���������� ������������� (HWND) ���� �������
HWND getConsoleHWND() {
    char title[256];
    GetConsoleTitle(title, 256);
    HWND hwndConsoleWindow;
    hwndConsoleWindow = FindWindow(NULL, title);
    if (hwndConsoleWindow == 0)
    {
        printf("ERROR: Can't find the window!\n");
        exit(0);
    }
    return hwndConsoleWindow;
}
//������� ��������� ��������� ������ � ����� �������
void linkSocketWindow(SOCK *s) {
    if ((WSAAsyncSelect(*s, getConsoleHWND(), 777, FD_ACCEPT) == SOCKET_ERROR))
        puts("ERROR: Socket <-> console linking failed!");
    else
        puts("Socket is now linked with console!");
}
//������������� WinSock, ������ � �������� ���������� ������, ������� ������ � ����� �������� � ������� ������
SOCK getWorkingServer(unsigned int connectionsLimit) {
    SOCK s = createSocket();
    //ip � ���� �������
    bindSocket(&s, getLocalIp(), "80");
    linkSocketWindow(&s);
    listen(s, connectionsLimit);
    puts("Done!");
    return s;
}
//��������� �������� ip-������
char *getLocalIp() {
    char *ad = hostName();
    HOSTENT *hostInfo = gethostbyname(ad);
    ad = inet_ntoa(*(struct in_addr*)hostInfo->h_addr);
    return ad;
}
//����� ��������� ������
unsigned long printLastError() {
    unsigned long errnom = GetLastError();
    printf("ERROR #%d: %s\n", errnom, strerror(errnom));
    return errnom;
}