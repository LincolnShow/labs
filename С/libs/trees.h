#include <stdio.h>
#include <stdlib.h>
typedef int T;
typedef struct Node {
    T data; 
    struct Node *left; 
    struct Node *right; 
    struct Node *parent; 
} Node;
typedef unsigned int COUNTER;
typedef unsigned long long llu;
typedef unsigned int intu;
Node *getFreeNode(T);
Node *getMinNode(Node*);
Node *getMaxNode(Node*);
Node *findNode(Node*, T);
void deleteNode(Node*);
void insert(Node**, T);
char isLeft(Node*);
intu countNodes(Node*);
void randFill(Node*, int);
intu howManyNodes(Node *, T);
llu treeSum(Node*);
intu countLeftNodes(Node*);
intu countRightNodes(Node*);
intu countLeaves(Node*);
char isLeaf(Node*);
llu leavesSum(Node*);
intu treeDepth(Node*, intu);
intu getTreeDepth(Node*);
intu nodesOnLevel(Node*, intu, intu);
intu getNodesOnLevel(Node*, intu);
void printDepthValues(Node*);

//��������� ������ ��� ����� ���� � ������� ��������� �� ����
Node* getFreeNode(T value) {
    Node* tmp = (Node*)malloc(sizeof(Node));
    tmp->left = tmp->right = NULL;
    tmp->parent = NULL;
    tmp->data = value;
    return tmp;
}
//����� ����� ��� ������� value � �������;
void insert(Node **head, T value) {
    if (!(*head)) {
        *head = getFreeNode(value);
        return;
    }
    if (value >= (*head)->data) {
        if ((*head)->right)
            insert(&(*head)->right, value);
        else {
            (*head)->right = getFreeNode(value);
            (*head)->right->parent = *head;
            return;
        }
    }
    else {
        if ((*head)->left)
            insert(&(*head)->left, value);
        else {
            (*head)->left = getFreeNode(value);
            (*head)->left->parent = *head;
            return;
        }
    }
}
//���������� ������������ ��������
Node *getMinNode(Node *root) {
    if (root->left)
        getMinNode(root->left);
    else
        return root;
}
//���������� ������������� ��������
Node *getMaxNode(Node *root) {
    static int maxVal = 0;
    if (root->right)
        getMaxNode(root->right);
    else
        return root;
}
//����� ��������� ��������; ���� ������� �� ������, ������������ NULL
Node *findNode(Node *root, T element) {
    if (element == root->data)
        return root;
    else if (element > root->data) {
        if (root->right)
            findNode(root->right, element);
        else
            return NULL;
    }
    else {
        if (root->left)
            findNode(root->left, element);
        else
            return NULL;
    }
}
//�������� ��������� ����
void deleteNode(Node *node) {
    //���� � ���� ��� ����������
    if (node->left && node->right) {
        node->right->parent = node->left;
        node->left->right = node->right;
        node->left->parent = node->parent;
        node->parent->left = node->left;
    }
    //���� � ���� ���� ���� ���� ���������, �� �������� ���� ��� �����������
    else if (node->left || node->right) {
        if (node->left) {
            node->left->parent = node->parent;
            if (isLeft(node))
                node->parent->left = node->left;
            else
                node->parent->right = node->left;
        }
        else {
            node->right->parent = node->parent;
            if (isLeft(node))
                node->parent->left = node->right;
            else
                node->parent->right = node->right;
        }
    }
    //���� � ���� ��� �����������
    else {
        if (isLeft(node))
            node->parent->left = NULL;
        else
            node->parent->right = NULL;
    }
    free(node);
}
//������, ����� ����� �������� ��� �������� node
char isLeft(Node *node) {
    if (!node || !node->parent)
        return -1;
    else if (node->parent->left != node)
        return 0;
    else
        return 1;
}
//���������� ������ ���������� ������ ������
intu countNodes(Node *node) {
    static COUNTER nodesCount = 0;
    if (node && !node->parent)
        nodesCount = 0;
    if (node) {
        nodesCount++;
        countNodes(node->left);
        countNodes(node->right);
    }
    else
        return nodesCount;
}
//���������� ������ ���������� ������� �� 1 �� 100
void randFill(Node *root, int size) {
    srand(time(0));
    for (int i = 0; i < size; i++)
        insert(&root, rand() % 100 + 1);
}
//���������� ���������� �����, �������� ������� ����� N
intu howManyNodes(Node *node, T N) {
    static COUNTER total = 0;
    if (node && !node->parent)
        total = 0;
    if (node) {
        if (node->data == N)
            total++;
        howManyNodes(node->left, N);
        howManyNodes(node->right, N);
    }
    else
        return total;
    
}
//���������� ����� �������� ���� �����
llu treeSum(Node *node) {
    static llu sum = 0;
    if (node && !node->parent)
        sum = 0;
    if (node) {
        sum += node->data;
        treeSum(node->left);
        treeSum(node->right);
    }
    else
        return sum;
}
//���������� ���������� ����� �������� ������
intu countLeftNodes(Node *node) {
    static COUNTER total = 0;
    if (node && !node->parent)
        total = 0;
    if (node) {
        if (isLeft(node) == 1)
            total++;
        countLeftNodes(node->left);
        countLeftNodes(node->right);
    }
    else
        return total;
}
//���������� ���������� ������ �������� ������
intu countRightNodes(Node *node) {
    static COUNTER total = 0;
    if (node && !node->parent)
        total = 0;
    if (node) {
        if (!isLeft(node))
            total++;
        countRightNodes(node->left);
        countRightNodes(node->right);
    }
    else
        return total;
}
//���������� ���������� ������� ������
intu countLeaves(Node *node) {
    static intu total = 0;
    if (node && !node->parent)
        total = 0;
    if (node) {
        if (isLeaf(node))
            total++;
        countLeaves(node->left);
        countLeaves(node->right);
    }
    else
        return total;
}
//������, �������� �� ���� ������
char isLeaf(Node *node) {
    if (!node)
        return -1;
    else if (node->left || node->right)
        return 0;
    else
        return 1;
}
//���������� ����� �������� ���� �������
llu leavesSum(Node *node)
{
    static llu sum = 0;
    if (node && !node->parent)
        sum = 0;
    if (node) {
        if (isLeaf(node))
            sum += node->data;
        leavesSum(node->left);
        leavesSum(node->right);
    }
    else
        return sum;
}
//���������� ������� ������; ��������� depth = 0
intu treeDepth(Node *node, intu depth) {
    if (node) {
        if (isLeaf(node))
            return depth;
        else {
            intu lDepth = treeDepth(node->left, depth + 1);
            intu rDepth = treeDepth(node->right, depth + 1);
            return lDepth > rDepth ? lDepth : rDepth;
        }
    }
    else
        return depth;
}
//������� ��� treeDepth
intu getTreeDepth(Node *node) {
    return treeDepth(node, 0);
}
//����� ���������� ����� �� ������ ������ ������
void printDepthValues(Node *node) {
    intu ndepth = getTreeDepth(node);
    for (int i = 0; i <= ndepth; i++) {
        printf("Level %d, nodes: %d\n", i, getNodesOnLevel(node, i));
    }
}
//���������� ���������� ����� �� �������� ������; ��������� pos = 0
intu nodesOnLevel(Node *node, intu level, intu pos) {
    static intu total = 0;
    if (node && !node->parent)
        total = 0;
    else if (level == 0)
        return 1;
    if (node) {
        if (pos == level)
            total++;
        else {
            nodesOnLevel(node->left, level, pos + 1);
            nodesOnLevel(node->right, level, pos + 1);
            return total;
        }
    }
    else
        return total;
}
//������� ��� nodesOnLevel
intu getNodesOnLevel(Node *node, intu level) {
    return nodesOnLevel(node, level, 0);
}