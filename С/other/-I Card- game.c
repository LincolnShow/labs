#include <stdio.h>
#include <locale.h>
struct player {
    int citizens;
};

char wantToPlay();
unsigned int getRounds();
void cleanIn();
char getSide();
char playerGoes(char, struct player *player);
char enemyGoes(char, struct player *enemy);
char compare(char player, char enemy);

int main() {
    setlocale(LC_ALL, "Russian");
    struct player player;
    player.citizens = 4;
    struct player enemy;
    enemy.citizens = 4;
    srand(time(0));

    while (wantToPlay()) {
        int rounds = getRounds();
        system("cls");
        char playerSide = getSide(); //Player's side: 0 - slave, 1 - emperor
        char playerFirst = playerSide; //Whether player goes first or not
        //Side change (every 3 rounds)
        for (int i = 0; i < rounds/3; i++)
        {
            //Round change
            for (int i = 0; i < 3; i++)
            {
                player.citizens = 4, enemy.citizens = 4;
                while (1) {
                    system("cls");
                    char playerCard = 0, enemyCard = 0;
                    if(playerFirst){
                        puts("�� ������ ������.");
                        playerCard = playerGoes(playerSide, &player);
                        printf("�� �������� %s.\n", playerCard == 'c' ? "����������" : (playerCard == 's' ? "����" : "����������"));
                        enemyCard = enemyGoes(!playerSide, &enemy);
                        puts("��������� ������ �����.");
                    }
                    else {
                        puts("��������� ����� ������.");
                        enemyCard = enemyGoes(!playerSide, &enemy);
                        puts("��������� ������ �����. ������ ��� ���.");
                        playerCard = playerGoes(playerSide, &player);
                        printf("�� �������� %s.\n", playerCard == 'c' ? "����������" : (playerCard == 's' ? "����" : "����������"));
                    }
                    puts("������� ����� �������, ����� ������� �����...\n");
                    getch();
                    char result = compare(playerCard, enemyCard);
                    if (result == 0)
                        puts("��������� ���� ������� ����������. �����.");
                    else if (result == 'w') {
                        printf("��������� ������� %s. �� �������� �����!\n", enemyCard == 'c' ? "����������" : (enemyCard == 's' ? "����" : "����������"));
                        puts("������� ����� ������� ��� �����������...");
                        getch();
                        break;
                    }
                    else if (result == 'l') {
                        printf("��������� ������� %s. �� ��������� �����...\n", enemyCard == 'c' ? "����������" : (enemyCard == 's' ? "����" : "����������"));
                        puts("������� ����� ������� ��� �����������...");
                        getch();
                        break;
                    }
                    puts("������� ����� ������� ��� �����������...");
                    getch();
                    playerFirst = !playerFirst;
                }
            }
            playerSide = !playerSide;
        }
        puts("������� ����� ������� ��� �����������...");
        getch();
    }
    return 0;
}
//Ask for another game
char wantToPlay() {
    system("cls");
    puts("�� ������ �������? (y/n)");
    char c;
    while (1) {
        c = getch();
        if (c == 'y')
            return 1;
        else if (c == 'n')
            return 0;
    }
}
//Ask for a number of rounds
unsigned int getRounds() {
    unsigned int rounds = 0;
    while (1) {
        system("cls");
        puts("������� ������� �� ������ �������? (����� ������ ���� ������ 3-�)");
        if (scanf("%ud", &rounds) == 1 && (rounds % 3 == 0))
            return rounds;
        cleanIn();
    }
}
//stdin clean
void cleanIn() {
    char c;
    do {
        c = getchar();
    } while (c != 0 && c != '\n');
}
//Set player's side: 0 - slave, 1 - emperor
char getSide() {
    return rand() % 2;
}
//Player makes a turn
char playerGoes(char side, struct player *player) {
    while (1) {
        cleanIn();
        printf("�� ������� �� ������� %s. �������� ����� (1 - ���������, 2 - %s):\n", side == 1 ? "����������" : "����", side == 1 ? "���������" : "���");
        printf("� ��� � ���� %d ���������� � %s.\n", (*player).citizens, side == 1 ? "���������" : "���");
        char c = 0;
        scanf("%c", &c);
        if (c == '1') {
            (*player).citizens--;
            return 'c';
        }
        else if (c == '2') {
            if (side == 1)
                return 'e';
            else if (side == 0)
                return 's';
        }
        system("cls");
    }
}
//Computer makes a turn
char enemyGoes(char side, struct player *enemy) {
    char c = rand() % ((*enemy).citizens + 1);
    if (side == 1) {
        if (c == 0) {
            return 'e';
        }
        else {
            (*enemy).citizens--;
            return 'c';
        }

    }
    else {
        if (c == 0)
            return 's';
        else {
            (*enemy).citizens--;
            return 'c';
        }
    }
}
//Compare two cards and tell the result: 0 - draw, w - player wins, l - player loses
char compare(char player, char enemy) {
    if (player == enemy)
        return 0;
    else if ((player == 'e' && enemy == 'c') ||
        (player == 's' && enemy == 'e') ||
        (player == 'c' && enemy == 's'))
        return 'w';
    else
        return 'l';
}
