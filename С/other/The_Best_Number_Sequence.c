/*
���������� ����� �� �������� ������ "����� � ����"
*/
#include <stdio.h>
#define N 10
#define M 10
void fillArr(int(*)[]);
void sum(int(*)[], int(*)[]);
int max(int, int);
void printArr(int(*)[]);
void findTheWay(int(*)[], int(*)[]);
void clearArr(int(*)[]);
int main() {
    int a[N][M];
    fillArr(a);
    printArr(a);
    int b[N][M];
    sum(a, b);
    puts("------------");
    findTheWay(a, b);
    return 0;
}
//���������� ������� ���������� ������� �� 1 �� 100
void fillArr(int(*array)[M]) {
    srand(time(0));
    for (int i = 0; i < N; i++) {
        for (int q = 0; q < M; q++) {
            array[i][q] = rand() % 100 + 1;
        }
    }
}
//���������� ������� ����
void sum(int(*a)[M], int(*b)[M]) {
    b[0][0] = a[0][0];
    //������������ �������
    for (int i = 1; i < N; i++) {
        b[i][0] = b[i - 1][0] + a[i][0];
    }
    //�������������� �������
    for (int j = 1; j < M; j++) {
        b[0][j] = b[0][j - 1] + a[0][j];
    }
    //����
    for (int i = 1; i < N; i++) {
        for (int j = 1; j < M; j++) {
            b[i][j] = max(b[i - 1][j], b[i][j - 1]) + a[i][j];
        }
    }
}
//��������� ��������� � ������� �������� �� ����
int max(int a, int b) {
    return a > b ? a : b;
}
//����� ������� �� �����
void printArr(int(*array)[M]) {
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
                printf("%3d ", array[i][j]);
        }
        putchar('\n');
    }
}
//���������� ������� ����
void findTheWay(int (*a)[M], int (*b)[M]) {
    int answer[N][M];
    answer[0][0] = a[0][0];
    clearArr(answer);
    int pos = answer[N - 1][M - 1] = a[N - 1][M - 1];
    int x = M - 1, y = N - 1;
    int upper, left;
    while (pos != b[0][0]) {
        upper = b[y-1][x];
        left = b[y][x - 1];
        if ((pos - a[y][x]) == upper) {
            y--;
        }
        else if((pos - a[y][x]) == left){
            x--;
        }
        answer[y][x] = a[y][x];
        pos = b[y][x];
    }
    printArr(answer);
}
//���������� ������� ������
void clearArr(int(*array)[M]) {
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            array[i][j] = 0;
        }
    }
}
