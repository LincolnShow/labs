#pragma comment(lib, "ws2_32.lib")
#include <stdio.h>

//������ � ��������
#include <winsock.h>
SOCKET sock(const char*);
int main() {
    //������� ����� s
    SOCKET s = sock("tcp");
    int listenResult = listener(sock, "192.168.0.104", "80");
    return 0;
}
//�������� ����� ��������� ����
SOCKET sock(const char *transport) {
    //struct protoent *ppe; 	//��������� �� ������ � ����������� � ���������
    SOCKET s, type;			//���������� � ��� ������
    int error;
    //��������������� ��� ������������� ��������� � ���������� ����� ���������
    //ppe = getprotobyname(transport);
    //���������� ��� ������, �������� ����� ������������� ��������� 	 
    if (strcmp(transport, "udp") == 0)
        type = SOCK_DGRAM;		//���� udp - ��������� SOCK_DGRAM
    else
        type = SOCK_STREAM;		//� ���� (tcp) ������ - ��������� SOCK_STREAM
    //������� ����� �����, ������� ����������� ���������
    s = socket(PF_INET, type, 0);
    //���������� ��������� - ���������� ������.
    return s;
}
/*
������� ��������� ���������� ������ sock, � ������� host � ������ port. 
��� �������������� host � port �� ��������� �������� (�������� "192.168.1.0" � "21") � ���������� �������� �������� ������������ ������� gethostbyname � htons. 
�������� ������ � ����� �������������� ���� ��������� sockaddr_in, ������� �������� ���������� ������� bind. ������� listener ���������� ��������� ������ ������� listen.
*/
int listener(SOCKET sock, const char *host, const char *port) {
    struct hostent     *phe; 	//��������� �� ������ � ����������� � �����
    struct sockaddr_in  sin; 	//��������� IP-������
    int errorID = 0;
    //�������� ��������� ������
    memset(&sin, 0, sizeof(sin));
    //��������� ��� ������ - IPv4, ��� IPv6 ���������� ������� AF_INET6
    sin.sin_family = AF_INET;
    //������ ����
    sin.sin_port = htons((unsigned short)atoi(port));
    //������ �����
    //��������������� ������ ������
    phe = gethostbyname(host);
    if (!phe) {
        errorID = WSAGetLastError();
    }
    //�������� �������� � ���� ��������� ������
    memcpy(&sin.sin_addr, phe->h_addr, phe->h_length);
    //��������� ����� � ��������� �������, �������� ��������� ������ �������
    if (bind(sock, (struct sockaddr *)&sin, sizeof(sin)) < 0)
        return -1; //� ������ ������� ���������� - 1
        //�������� ����� ������������� ��� 5 ��������, ��������� ���������
        return listen(sock, 5);
}