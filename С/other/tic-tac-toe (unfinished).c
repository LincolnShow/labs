#include <stdio.h>
//����� ���� �� �����
void showField(unsigned char(*field)[5]) {
    system("cls");
    for (int i = 0; i < 5; i++) {
        printf("                                  ");
        for (int q = 0; q < 5; q++)
        {
            putchar(field[i][q]);
        }
        putchar('\n');
    }
}
//���� ���� ����������, �� 1; ����� - 0
char checkWin(unsigned char(*field)[5]) {
        //��������������
    if ((field[0][0] == field[0][2] && field[0][2] == field[0][4]) ||
        (field[2][0] == field[2][2] && field[2][2] == field[2][4]) ||
        (field[4][0] == field[4][2] && field[4][2] == field[4][4]) ||
        //������������
        (field[0][0] == field[2][0] && field[2][0] == field[4][0]) ||
        (field[0][2] == field[2][2] && field[2][2] == field[4][2]) ||
        (field[0][4] == field[2][4] && field[2][4] == field[4][4]) ||
        //���������
        (field[0][0] == field[2][2] && field[2][2] == field[4][4]) ||
        (field[0][4] == field[2][2] && field[2][2] == field[4][0]))
        return 1;
    return 0;
}
//������ ������
void cleanIn() {
    char ch;
    do {
        ch = getchar();
    } while (ch != 0 && ch != '\n');
}
//�������� ��������� ������: 0 - ������, 1 - ��������
char checkCell(unsigned char *cell) {
    if (cell == -1)
        return 0;
    else if ((*cell) > '0' && (*cell) < '10')
        return 1;
    return 0;
}
//��������� ������ � ��������� �������
unsigned char *getCell(char cell, unsigned char(*field)[5]) {
    switch (cell) {
    case(1) :
        return &field[0][0];
    case(2):
        return &field[0][2];
    case(3):
        return &field[0][4];
    case(4):
        return &field[2][0];
    case(5):
        return &field[2][2];
    case(6):
        return &field[2][4];
    case(7):
        return &field[4][0];
    case(8):
        return &field[4][2];
    case(9):
        return &field[4][4];
    }
    return -1;
}
//����� ������� �����
char isPlayerFirst() {
    puts("Hello! Would you like to go first? (y/n)");
    while (1) {
        char c = getch();
        if (c == 'y')
            return 1;
        else if (c == 'n')
            return 0;
    }
}
//MAIN
int main() {
    unsigned char playfield[5][5] = { 
                                    {'1', '|', '2', '|', '3'},
                                    {'-', '|', '-', '|', '-'},
                                    {'4', '|', '5', '|', '6'},
                                    {'-', '|', '-', '|', '-'},
                                    {'7', '|', '8', '|', '9'},
                                    };
    unsigned char mark = 0, turn;
    if (isPlayerFirst()) {
        mark = 'X';
        turn = 1;
    }
    else {
        mark = 'O';
        turn = 0;
    }
    showField(playfield);
    while (!checkWin(playfield)) {
        if (1) {
            while (1) {
                printf("You play as %c.\nSelect a cell to make your turn (enter a number):", mark);
                char mcell;
                if (scanf("%d", &mcell) == 1 && checkCell(getCell(mcell, playfield))) {
                    *getCell(mcell, playfield) = mark;
                    break;
                }
                else {
                    cleanIn();
                }
            }
        }
        showField(playfield);
        turn = !turn;
    }
    printf("%c wins!\n", mark);
    
    return 0;
}