#include <stdio.h>
#include <string.h>
#include <locale.h>
#define SIZE 28
#define SIZE_H 9
void moveLeft(int *curY, int *curX, char *sightDirection, char(*maze)[SIZE]);
void moveForward(int *curY, int *curX, char *sightDirection, char(*maze)[SIZE]);
void moveRight(int *curY, int *curX, char *sightDirection, char(*maze)[SIZE]);
void printMaze(char(*mazef)[SIZE]);
char checkFront(int *curYf, int *curXf, char *sightDirectionf, char(*mazef)[SIZE]);
char checkLeft(int *curYf, int *curXf, char *sightDirectionf, char(*mazef)[SIZE]);
char checkRight(int *curYf, int *curXf, char *sightDirectionf, char(*mazef)[SIZE]);
void cleanIn();
char scanArrow();
unsigned char scanArrow();
int main() {
    char maze[SIZE_H][SIZE] = {
        { '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#' },
        { '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#' },
        { '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', '#', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#' },
        { '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', '#', '#', '#' },
        { '#', ' ', '#', '#', '#', '#', '#', '#', ' ', ' ', ' ', ' ', '#', ' ', '^', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#' },
        { '#', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', ' ', '#', '#' },
        { '#', '#', '#', '#', '#', '#', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', '#' },
        { '#', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#' },
        { '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#' } };
    int curY = 4, curX = 14;
    char sightDirection = 1;
    int *curYp = &curY, *curXp = &curX;
    char *sDp = &sightDirection;
    setlocale(LC_ALL, "Russian");
    while (1) {
        printMaze(maze);
        puts("Use arrow keys to move!");
        char c = scanArrow();
        if (c != -1) {
            if (c == 1) {
                sightDirection = 1;
                char check = checkFront(curYp, curXp, sDp, maze);
                if (check == 1 || check == 2)
                    moveForward(curYp, curXp, sDp, maze);
                else if (check == 3)
                    break;
            }
            else if (c == 4) {
                sightDirection = 4;
                char check = checkFront(curYp, curXp, sDp, maze);
                if (check == 1 || check == 2)
                    moveForward(curYp, curXp, sDp, maze);
                else if (check == 3)
                    break;
            }
            else if (c == 2) {
                sightDirection = 2;
                char check = checkFront(curYp, curXp, sDp, maze);
                if (check == 1 || check == 2)
                    moveForward(curYp, curXp, sDp, maze);
                else if (check == 3)
                    break;
            }
            else if (c == 3) {
                sightDirection = 3;
                char check = checkFront(curYp, curXp, sDp, maze);
                if (check == 1 || check == 2)
                    moveForward(curYp, curXp, sDp, maze);
                else if (check == 3)
                    break;
            }
        }
    }
    moveForward(curYp, curXp, sDp, maze);
    printMaze(maze);
    return 0;
}
//������� ������
void cleanIn() {
    char c;
    do {
        c = getchar();
    } while (c != 0 && c != '\n');
}
//������� ��������� ���� �� ������� ������� ����� � ���������� �������� � ����������� �� ������� �������; ���� ���� ������ �� �������, �� ���������� -1
char scanArrow() {
    unsigned char c = getch();
    if (c == 224) {
        c = getch();
        if (c == 'H')
            return 1; //^
        else if (c == 'P')
            return 3; //v
        else if (c == 'M')
            return 2; //>
        else if (c == 'K')
            return 4; //<
    }
    else {
        return -1;
    }
}
//�������� � ��������� ����������� �� ��������� � �������
void moveForward(int *curYf, int *curXf, char *sightDirectionf, char(*mazef)[SIZE]) {
    mazef[*curYf][*curXf] = 0;
    if ((*sightDirectionf) == 1) {
        (*curYf)--;
        mazef[*curYf][*curXf] = '^';
    }
    else if ((*sightDirectionf) == 2) {
        (*curXf)++;
        mazef[*curYf][*curXf] = '>';
    }
    else if ((*sightDirectionf) == 3) {
        (*curYf)++;
        mazef[*curYf][*curXf] = 'v';
    }
    else if ((*sightDirectionf) == 4) {
        (*curXf)--;
        mazef[*curYf][*curXf] = '<';
    }
}
//�������� ������: 0 - �����; 1 - ������������ �����; 2 - ���������� �����; 3 - �����
char checkFront(int *curYf, int *curXf, char *sightDirectionf, char(*mazef)[SIZE]) {
    if (*sightDirectionf == 1) {
        if (mazef[*curYf - 1][*curXf] != '#' && (((*curYf - 1) == SIZE_H - 1) || ((*curYf - 1) == 0)))
            return 3;
        else if (mazef[*curYf - 1][*curXf] == ' ')
            return 1;
        else if (mazef[*curYf - 1][*curXf] == 0)
            return 2;
        else
            return 0;
    }
    else if (*sightDirectionf == 2) {
        if (mazef[*curYf][*curXf + 1] != '#' && (((*curXf + 1) == SIZE - 1) || ((*curXf + 1) == 0)))
            return 3;
        else if (mazef[*curYf][*curXf + 1] == ' ')
            return 1;
        else if (mazef[*curYf][*curXf + 1] == 0)
            return 2;
        else
            return 0;
    }
    else if (*sightDirectionf == 3) {
        if (mazef[*curYf + 1][*curXf] != '#' && (((*curYf + 1) == SIZE_H - 1) || ((*curYf + 1) == 0)))
            return 3;
        else if (mazef[*curYf + 1][*curXf] == ' ')
            return 1;
        else if (mazef[*curYf + 1][*curXf] == 0)
            return 2;
        else
            return 0;
    }
    else if (*sightDirectionf == 4) {
        if (mazef[*curYf][*curXf - 1] != '#' && (((*curXf - 1) == SIZE - 1) || ((*curXf - 1) == 0)))
            return 3;
        else if (mazef[*curYf][*curXf - 1] == ' ')
            return 1;
        else if (mazef[*curYf][*curXf - 1] == 0)
            return 2;
        else
            return 0;
    }
    return -1;
}
//����� ��������� �� ����� � ��������
void printMaze(char(*mazef)[SIZE]) {
    system("cls");
    for (int i = 0; i < SIZE_H; i++)
    {
        for (int q = 0; q < SIZE; q++)
        {
            putchar(mazef[i][q]);
        }
        putchar('\n');
    }
}