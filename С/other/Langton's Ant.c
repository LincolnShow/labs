//��������� ������� - ������� ��������
#include <stdio.h>
#include <Windows.h>
#define SIZE 26
int main() {
    char space[SIZE][SIZE];
    for (int i = 0; i < SIZE; i++)
    {
        for (int q = 0; q < SIZE; q++) {
            space[i][q] = '.';
        }
    }
    char ant;
    char antsFront = 1;
    short curY = SIZE / 2, curX = SIZE / 2;
    char curChar = '.';
    // '.' - �����, 'x' - ������
    //              1^
    //          4<     >2
    //              3v
    while (1) 
    {
        system("cls");
        curChar = space[curY][curX];
        //���� ����� �� ����� �������
        if (curChar == '.') {
            antsFront++;
            if (antsFront > 4)
                antsFront = 1;
            else if (antsFront < 1)
                antsFront = 4;

            space[curY][curX] = 'x';
            
        }
        //���� ����� �� ������ �������
        else if (curChar == 'x') {
            antsFront--;
            if (antsFront > 4)
                antsFront = 1;
            else if (antsFront < 1)
                antsFront = 4;

            space[curY][curX] = '.';
        }

        curChar = space[curY][curX];
        if (antsFront == 1) {
            ant = '^';
            curY--;
        }
        else if (antsFront == 2) {
            ant = '>';
            curX++;
        }
        else if (antsFront == 3) {
            ant = 'v';
            curY++;
        }
        else if (antsFront == 4) {
            ant = '<';
            curX--;
        }
        else {
            puts("Error");
            break;
        }

        if (curY >= SIZE)
            curY = 0;
        else if (curY < 0)
            curY = SIZE - 1;
        if (curX >= SIZE)
            curX = 0;
        else if (curX < 0)
            curX = SIZE - 1;
        

        //����� �� �����
        for (int i = 0; i < SIZE; i++)
        {
            for (int q = 0; q < SIZE; q++) {
                if(i != curY || q != curX)
                    putchar(space[i][q]);
                else
                    putchar(ant);
            }
            putchar('\n');
        }

        Sleep(1);
    }
    return 0;
}