#include "functions.h"
#include <time.h>
int main(int argc, char *argv[]) {
    double start = clock();
    if (argc < 2) {
        printf("Usage: PROGRAM_NAME SOURCE_FILE_PATH");
        exit(1);
    }
    FILE *src = fopen(argv[1], "rb");
    char packedFilePath[256] = { 0 };
    strcat(packedFilePath, argv[1]);
    strcat(packedFilePath, ".pkd");
    FILE *dst = fopen(packedFilePath, "wb");
    if (!src) {
        puts("File open error!");
        exit(2);
    }
    if (!dst) {
        puts("File creation error!");
        exit(3);
    }
    int uniques = getUniques(src);
    if (uniques < 2) {
        puts("Not enough unique symbols for compressing!");
        exit(4);
    }
    unsigned int tailLength = 0, total = 0;
    //making SYM-array
    SYM *syms = (SYM*)malloc(sizeof(SYM)*256);
    SYM **psyms = (SYM**)malloc(sizeof(SYM*)*uniques);
    resetSyms(syms, 256);
    //frequency table
    total = buildFreqTable(syms, 256, src);
    sortSyms(syms, 256);
    fillpArr(psyms, syms, 256);
    //tree and the codes
    SYM *root = buildTree(psyms, uniques);
    makeCodes(root);

    //Making packed file
    //HEADER
    //1 - signature
    fprintf(dst, "%s", "PKD ");
    //2 - number of unique elements in the frequency table
    fputc(uniques, dst);
    //3 - frequency table
    for (unsigned int i = 0; i < uniques; i++) {
        fwrite(&syms[i].ch, sizeof(char), 1, dst);
        fwrite(&syms[i].freq, sizeof(float), 1, dst);
    }
    fputc(' ', dst);
    //4 - size of the original file
    fprintf(dst, "%d ", total);
    //5 - extension of the original file
    char extension[16] = { 0 };
    while (strstr(argv[1], ".") != NULL) {
        argv[1]++;
    }
    fprintf(dst, "%s ", argv[1]);
    //Packing
    int c = 0, pos = 0;
    char flag = 0;
    unsigned char tmpByte[8] = { 0 };
    rewind(src);
    while ((flag = 1) && (c = fgetc(src)) != EOF) {
        for (unsigned int i = 0; i < uniques; i++) {
            if (c == syms[i].ch) {
                for (unsigned int q = 0; q < strlen(syms[i].code); q++) {
                    tmpByte[pos++] = syms[i].code[q];
                    if (pos > 7) {
                        pos = 0;
                        fputc(pack(tmpByte), dst);
                        flag = 0;
                        memset(tmpByte, '0', 8);
                    }
                }
                break;
            }
        }
    }
    if (flag)
        fputc(pack(tmpByte), dst);
    puts("Pack finished!");
    fcloseall();
    double end = clock();
    printf("Time elapsed: %.2lf sec\n", ((end - start) / CLOCKS_PER_SEC));
    return 0;
}