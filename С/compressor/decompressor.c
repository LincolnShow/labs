#include "functions.h"
#include <time.h>
unsigned char findCharByCode(SYM **node, CODE *chr, unsigned char startPos);
char  isLeaf(SYM *node);
char isTail(unsigned int tailLength, CODE *chr);

int main(int argc, char *argv[]) {
    double start = clock();
    if (argc < 2) {
        puts("Usage: PROGRAM_NAME COMPRESSED_FILE_PATH");
        exit(1);
    }
    FILE *compSrc = fopen(argv[1], "rb");
    if (!compSrc) {
        puts("File open error!");
        exit(2);
    }
    char ext[4] = { 0 };
    //Reading header
    //1 - signature
    fscanf(compSrc, "%s", ext);
    if (strcmp("PKD", ext)) {
        puts("Wrong file signature!");
        exit(3);
    }
    int uniques = 0;
    unsigned int tailLength = 0, fileSize = 0;
    char fileExtension[16] = { 0 };
    //2 - the number of unique elements in the frequency table
    fscanf(compSrc, "%*c%cu", &uniques);
    if (!uniques)
        uniques = 256;
    SYM *syms = (SYM*)malloc(sizeof(SYM) * uniques);
    SYM **psyms = (SYM**)malloc(sizeof(SYM*) * uniques);
    fillpArr(psyms, syms, uniques);
    resetSyms(syms, uniques);
    //3 - frequency table
    for (unsigned int i = 0; i < uniques; i++) {
        fread(&syms[i].ch, sizeof(char), 1, compSrc);
        fread(&syms[i].freq, sizeof(float), 1, compSrc);
    }
    //4 - size of the original file
    fscanf(compSrc, "%d", &fileSize);
    //5 - extension of the original file
    fscanf(compSrc, "%s", fileExtension);
    fgetc(compSrc);
    //Making path for the decompressed file
    char originalFilePath[256] = { 0 };
    strcat(originalFilePath, argv[1]);
    strcat(originalFilePath, ".");
    strcat(originalFilePath, fileExtension);
    FILE *decompDst = fopen(originalFilePath, "w+b");
    if (!decompDst) {
        puts("File creation error!");
        exit(4);
    }
    SYM *root = buildTree(psyms, uniques);
    makeCodes(root);
    SYM *node = root;
    CODE chr;
    int nextChr;
    chr.ch = fgetc(compSrc);
    nextChr = fgetc(compSrc);
    unsigned char pos = 1;
    unsigned int decompSize = 0;

    while (decompSize < (fileSize)) {
        pos = findCharByCode(&node, &chr, pos);
        if (pos == 1) {
            chr.ch = nextChr;
            nextChr = fgetc(compSrc);
        }
        if (isLeaf(node)) {
            fputc(node->ch, decompDst);
            decompSize++;
            fflush(decompDst);
            node = root;
        }
    }
    fclose(compSrc);
    puts("Unpack finished.");
    if (decompSize != fileSize) {
        puts("WARNING: size of the unpacked file is different from the original file!");
    }
    double end = clock();
    printf("Time elapsed: %.2lf sec\n", ((end - start) / CLOCKS_PER_SEC));
    return 0;
}
//Find a char by its code value; return start position of the next search
unsigned char findCharByCode(SYM **node, CODE *chr, unsigned char startPos) {
    if (startPos <= 1 && !isLeaf((*node))) {
        startPos++;
        if (chr->byte.b1) {
            if ((*node)->right)
                *node = (*node)->right;
        }
        else {
            if ((*node)->left)
                (*node) = (*node)->left;
        }
    }
    if (startPos <= 2 && !isLeaf((*node))) {
        startPos++;
        if (chr->byte.b2) {
            if ((*node)->right)
                (*node) = (*node)->right;
        }
        else {
            if ((*node)->left)
                (*node) = (*node)->left;
        }
    }
    if (startPos <= 3 && !isLeaf((*node))) {
        startPos++;
        if (chr->byte.b3) {
            if ((*node)->right)
                (*node) = (*node)->right;
        }
        else {
            if ((*node)->left)
                (*node) = (*node)->left;
        }
    }
    if (startPos <= 4 && !isLeaf((*node))) {
        startPos++;
        if (chr->byte.b4) {
            if ((*node)->right)
                (*node) = (*node)->right;
        }
        else {
            if ((*node)->left)
                (*node) = (*node)->left;
        }
    }
    if (startPos <= 5 && !isLeaf((*node))) {
        startPos++;
        if (chr->byte.b5) {
            if ((*node)->right)
                (*node) = (*node)->right;
        }
        else {
            if ((*node)->left)
                (*node) = (*node)->left;
        }
    }
    if (startPos <= 6 && !isLeaf((*node))) {
        startPos++;
        if (chr->byte.b6) {
            if ((*node)->right)
                (*node) = (*node)->right;
        }
        else {
            if ((*node)->left)
                (*node) = (*node)->left;
        }
    }
    if (startPos <= 7 && !isLeaf((*node))) {
        startPos++;
        if (chr->byte.b7) {
            if ((*node)->right)
                (*node) = (*node)->right;
        }
        else {
            if ((*node)->left)
                (*node) = (*node)->left;
        }
    }
    if (startPos <= 8 && !isLeaf((*node))) {
        startPos = 1;
        if (chr->byte.b8) {
            if ((*node)->right)
                (*node) = (*node)->right;
        }
        else {
            if ((*node)->left)
                (*node) = (*node)->left;
        }
    }
    return startPos;
}
//leaf check
char  isLeaf(SYM *node) {
    if (!node->left && !node->right)
        return 1;
    return 0;
}