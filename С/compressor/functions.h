#include <string.h>
#include <stdlib.h>
#include <stdio.h>
typedef struct SYM {
    unsigned char ch; //ASCII-code
    float freq; //frequency table
    char code[256]; //New code array
    struct SYM *left; //left child
    struct SYM *right; //right child
} SYM;
typedef union CODE {
    unsigned char ch;
    struct {
        unsigned short b1 : 1;
        unsigned short b2 : 1;
        unsigned short b3 : 1;
        unsigned short b4 : 1;
        unsigned short b5 : 1;
        unsigned short b6 : 1;
        unsigned short b7 : 1;
        unsigned short b8 : 1;
    }byte;
}CODE;
//count total unique symbols in the file
int getUniques(FILE* fp);
//Nullify elements of SYM-array
void resetSyms(SYM* syms, unsigned int size);
//Fill SYM*-array
void fillpArr(SYM** pointers, SYM* arr, unsigned int size);
//Fill frequency table; return total number of bytes in the file
int buildFreqTable(SYM* syms, unsigned int size, FILE* fp);
//Build a binary tree
SYM *buildTree(SYM** psyms, unsigned int N);
//Make codes
void makeCodes(SYM *root);
//Pack 8 bits into 1 byte
char pack(unsigned char *arr);
//Sort SYM-array
void sortSyms(SYM *syms, int size);